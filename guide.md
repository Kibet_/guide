
#### To delete your account, please send an email to kibetedgar@gmail.com with the following subject line:

Request to delete account

In the body of the email, please include the following information:

Your name.  
Your email address. 
The reason you are deleting your account (optional). 
Once we receive your request, we will double-check your account before deleting it. This process may take up to 48 hours.  

Please note that once your account is deleted, it cannot be recovered.